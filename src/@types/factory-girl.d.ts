// Type definitions for factory-girl 5.0
// Project: https://github.com/aexmachina/factory-girl#readme
// Definitions by: Stack Builders <https://github.com/stackbuilders>
//                 Sebastián Estrella <https://github.com/sestrella>
//                 Luis Fernando Alvarez <https://github.com/elcuy>
//                 Olivier Kamers <https://github.com/OlivierKamers>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

declare module "factory-girl" {
  export = factory;

  namespace factory {
    export interface FactoryGirl {
      /**
       * Associate the factory to other model
       */
      assoc(model: string, attributes: string): any;

      /**
       * Associate the factory to a model that's not persisted
       */
      assocAttrs(name: string, key?: string, attributes?: any): any;

      /**
       * Associate the factory to multiple other models
       */
      assocMany(model: string, num: number, attributes: string): any[];

      /**
       * Generates and returns model attributes as an object hash instead of the model instance
       */
      attrs<T>(name: string, attrs?: Attributes<Partial<T>>): Promise<T>;

      /**
       * Generates and returns a collection of model attributes as an object hash instead of the model instance
       */
      attrsMany<T>(name: string, num: number, attrs?: ReadonlyArray<Attributes<Partial<T>>>): Promise<T[]>;

      /**
       * Builds a new model instance that is not persisted
       */
      build<T>(name: string, attrs?: Attributes<Partial<T>>, buildOptions?: Options<T>): Promise<T>;

      /**
       * Builds an array of model instances that are persisted
       */
      buildMany<T>(name: string, num: number, attrs?: Attributes<Partial<T>>): Promise<T[]>;
      buildMany<T>(name: string, attrs?: ReadonlyArray<Attributes<Partial<T>>>): Promise<T[]>;

      /**
       * Destroys all of the created models
       */
      cleanUp(): void;

      /**
       * Builds a new model instance that is persisted
       */
      create<T>(name: string, attrs?: Attributes<Partial<T>>, buildOptions?: Options<T>): Promise<T>;

      /**
       * Builds an array of model instances that are persisted
       */
      createMany<T>(name: string, num: number, attrs?: Attributes<Partial<T>>, buildOptions?: Options<T>): Promise<T[]>;
      createMany<T>(
        name: string,
        attrs?: ReadonlyArray<Attributes<Partial<T>>>,
        buildOptions?: Options<T>
      ): Promise<T[]>;

      /**
       * Define a new factory with a set of options
       */
      define<T>(name: string, model: any, attrs: Attributes<T> | any, options?: Options<T>): void;

      /**
       * Extends a factory
       */
      extend(parent: string, name: string, initializer: any, options?: Options<any>): any;

      /**
       * Generate values sequentially inside a factory
       */
      seq<T>(name: string, fn: (sequence: number) => T): Generator<T>;
      sequence<T>(name: string, fn: (sequence: number) => T): Generator<T>;

      /**
       * Register an adapter, either as default or tied to a specific model
       */
      setAdapter(adapter: Adapter, name?: string): void;

      /**
       *  Reset sequence generator with the given name
       *  or all generators if no name is given.
       */
      resetSequence(name?: string): void;
      resetSeq(name?: string): void;
    }

    export type Generator<T> = () => T;

    export type Definition<T> = T | Generator<T>;

    export type Attributes<T> = Definition<
      {
        [P in keyof T]: Definition<T[P]>;
      }
    >;

    export interface Options<T> {
      afterBuild?: Hook<T>;
      afterCreate?: Hook<T>;
    }

    export interface Adapter {
      build: (Model: any, props: any) => any;
      save: () => any;
      destroy: () => any;
      get: (model: any, attr: any, Model: any) => any;
      set: (props: any, model: any, Model: any) => any;
    }
    export class ObjectAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }
    export class DefaultAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }
    export class BookshelfAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }
    export class MongooseAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }
    export class SequelizeAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }
    export class ReduxORMAdapter implements Adapter {
      build(Model: any, props: any): void;
      save(): void;
      destroy(): void;
      get(model: any, attr: any, Model: any): void;
      set(model: any, attr: any, Model: any): void;
    }

    export type Hook<T> = (model: any, attrs: T[], options: any) => void;

    export const factory: FactoryGirl;
  }
}
