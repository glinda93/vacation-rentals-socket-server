import * as express from "express";
import * as cors from "cors";
import * as logger from "morgan";
import * as Routers from "./routes";
const app = express();

app.use(
  cors({
    origin: ["http://localhost:3000", "https://vacation.rentals", /\.vacation.rentals$/],
    optionsSuccessStatus: 200,
    credentials: true,
  })
);
app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/notifications", Routers.NotificationsRouter);

export default app;
