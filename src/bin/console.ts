import * as yargs from "yargs";
import { hideBin } from "yargs/helpers";
import * as jwt from "jsonwebtoken";
import { config } from "../config/helper";
import { connect } from "../helpers/socket";
import { SocketEvents } from "../socket/events";
import { ChatRoom } from "../db/models";
const debug = require("debug")("socket-server:console");

const argv = yargs(hideBin(process.argv))
  .usage("Usage: $0 <cmd> [options]")
  .command("send", "Send a message to user")
  .option("sender", {
    description: "Message sender id",
    default: "guest:dummy",
    alias: "s",
  })
  .option("receiver", {
    description: "User id to receive messages",
    default: "user:10001",
    alias: "r",
  })
  .option("message", {
    description: "Message content",
    alias: "m",
    demandOption: true,
  })
  .option("h", {
    alias: "help",
    description: "display help message",
  }).argv;

const { sender, receiver, message } = argv as { sender: string; receiver: string; message: string };

async function sendMessage(senderId: string, receiverId: string, message: string) {
  debug(`Sender ID: ${senderId}, Receiver ID: ${receiverId}, Message: ${message}`);
  let socket: SocketIOClient.Socket;
  if (senderId.startsWith("user:")) {
    const userId = senderId.replace("user:", "");
    const token = jwt.sign({ sub: userId }, config("app.jwt_secret"));
    debug(JSON.stringify({ userId, token }));
    socket = connect({ token });
  } else {
    debug(JSON.stringify({ guestId: senderId }));
    socket = connect({ guestId: senderId });
  }
  socket.on("connection", async () => {
    debug("Connected to socket");
    const room = await ChatRoom.findOrCreateByUserIds([senderId, receiverId]);
    const roomName = room.getName();
    debug(`Sending message to ${roomName}`);
    socket.emit(SocketEvents.SEND_MESSAGE, {
      roomName: room.getName(),
      message,
    });
    debug("Sent message");
  });
}

sendMessage(sender, receiver, message);
