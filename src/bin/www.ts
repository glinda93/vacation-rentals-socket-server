import app from "../app";
import * as http from "http";
import * as https from "https";
import * as fs from "fs";
import * as SocketIO from "socket.io";
import initSocket from "../socket";
import { config } from "../config/helper";
export const server = createServer(app);
export const io = new SocketIO.Server(server, {
  cors: {
    origin: ["http://localhost:3000", "https://vacation.rentals", /\.vacation.rentals$/],
    credentials: true,
  },
});
const debug = require("debug")("socket-server:server");
const port = normalizePort(config("app.port"));

app.set("port", port);
initSocket(io);

app.use(function (req, res, next) {
  res.locals.socketIO = io;
  next();
});

server.on("error", onError);
server.on("listening", onListening);
server.listen(port);

function createServer(app) {
  if (process.env.HTTPS === "true" && process.env.CERT_FILE && process.env.KEY_FILE) {
    const SSLOption = {
      key: fs.readFileSync(process.env.KEY_FILE),
      cert: fs.readFileSync(process.env.CERT_FILE),
    };
    return https.createServer(SSLOption, app);
  } else {
    return http.createServer(app);
  }
}

function normalizePort(val: any): number | boolean | any {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  debug("Listening on " + bind);
}
