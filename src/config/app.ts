import { env } from "../helpers";

export const app = {
  port: env("SOCKET_SERVER_PORT", "8888"),
  jwt_secret: env("JWT_SECRET", ""),
};