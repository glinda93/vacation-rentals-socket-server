import { env } from "../helpers";

export const db = {
  host: env("DB_HOST", "localhost"),
  port: env("DB_PORT", "3306"),
  database: env("DB_DATABASE", "vacarent"),
  username: env("DB_USERNAME", "user"),
  password: env("DB_PASSWORD", "password"),
  dialect: env("DB_DIALECT", "mysql"),
  logging: env("DB_LOGGING", false)
};
