import * as fs from "fs";
import * as appRoot from "app-root-path";
export enum NODE_ENV {
  DEVELOPMENT = "development",
  TEST = "test",
  PRODUCTION = "production",
}

export function getEnvironment(): NODE_ENV {
  const envs = [NODE_ENV.DEVELOPMENT, NODE_ENV.TEST, NODE_ENV.PRODUCTION];

  const nodeEnv: string = process.env.NODE_ENV || "";

  if (nodeEnv && envs.includes(nodeEnv as NODE_ENV)) {
    return nodeEnv as NODE_ENV;
  }

  return NODE_ENV.DEVELOPMENT;
}

export function getEnvFileName(): string {
  const appDir = appRoot.toString();
  const nodeEnv = getEnvironment();
  const filePath = `${appDir}/.env.${nodeEnv}`;
  if (fs.existsSync(filePath)) {
    return `.env.${nodeEnv}`;
  } else {
    console.warn(`File not found: ${filePath}`);
  }
  return ".env";
}
