import * as Config from "../config";
import * as _ from "lodash";

export const config = (key: string): any => {
  return _.get(Config, key);
};
