import * as fs from "fs";
import { getEnvFileName } from "./env";
console.log("loading config in " + getEnvFileName());

require("dotenv").config({ path: `${getEnvFileName()}` });

export * from "./app";
export * from "./db";
