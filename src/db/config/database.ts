import * as fs from "fs";
import { config } from "../../config/helper";

const dbConfig = config("db");

module.exports = {
  development: dbConfig,
};
