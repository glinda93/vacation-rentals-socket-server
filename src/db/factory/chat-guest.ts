import { factory } from "factory-girl";
import * as faker from "faker";
import { ChatGuest } from "../models";

factory.define("ChatGuest", ChatGuest, () => ({
  guestId: `guest:${faker.random.uuid()}`,
  name: `${faker.name.firstName()} ${faker.name.lastName()}`,
  ip: faker.internet.ip(),
}));
