import factory from "../../db/factory";
import * as faker from "faker";
import { ChatGuest, ChatMessage, ChatRoom } from "../models";

factory.define(
  "ChatMessage",
  ChatMessage,
  () => ({
    message: faker.lorem.sentence(),
  }),
  {
    afterBuild: async (model: ChatMessage, attrs: any, buildOptions) => {
      if (!model.userId) {
        const user: ChatGuest = await factory.create("ChatGuest");
        model.userId = user.guestId;
      }
      if (!model.roomId) {
        const room: ChatRoom = await ChatRoom.createFromUserIds([model.userId]);
        model.roomId = room.id;
      }
      return model;
    },
  }
);
