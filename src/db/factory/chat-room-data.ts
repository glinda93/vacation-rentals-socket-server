import factory from "../../db/factory";
import * as faker from "faker";
import { ChatGuest, ChatRoom, ChatRoomData } from "../models";

factory.define(
  "ChatRoomData",
  ChatRoomData,
  () => ({
    name: faker.lorem.word,
    lastReadAt: new Date(),
  }),
  {
    afterBuild: async (model) => {
      if (!model.userId) {
        const user: ChatGuest = await factory.create("ChatGuest");
        model.userId = user.guestId;
      }
      if (!model.roomId) {
        const room: ChatRoom = await factory.create("ChatRoom");
        model.roomId = room.id;
      }
      return model;
    },
  }
);
