import { factory, SequelizeAdapter } from "factory-girl";

const adapter = new SequelizeAdapter();
factory.setAdapter(adapter);

export default factory;

import "./chat-guest";
import "./chat-message";
import "./chat-room";
import "./chat-room-data";
