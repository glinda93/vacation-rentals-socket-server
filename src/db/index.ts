import { Sequelize } from "sequelize";
import { config } from "../config/helper";
const debug = require("debug")("socket-server:database");

class DatabaseManager {
  protected static _INSTANCE: DatabaseManager = null;
  protected db: Sequelize;
  protected constructor() {
    const { host, port, database, username, password } = config("db");
    debug("Creating sequelize instance, database: ", )
    this.db = new Sequelize(database, username, password, {
      host: host,
      port: port,
      dialect: "mysql",
    });
    debug("Sequelize instance created");
    debug("Dbname: " + this.db.getDatabaseName());
  }
  static getInstance() {
    if (!this._INSTANCE) {
      this._INSTANCE = new DatabaseManager();
    }
    return this._INSTANCE;
  }
  async init(): Promise<Sequelize> {
    try {
      await this.db.authenticate();
      debug("Connected to database");
    } catch (e) {
      throw new Error("Cannot connect to database");
    }
    return this.db;
  }
  getDb(): Sequelize {
    return this.db;
  }
}

export default DatabaseManager;
