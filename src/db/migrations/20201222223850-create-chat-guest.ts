import { DataTypes, QueryInterface } from "sequelize";
import { ChatGuestSchema, ChatGuestTableName } from "../models";

export default {
  up: async (queryInterface: QueryInterface) => {
    queryInterface.createTable(ChatGuestTableName, ChatGuestSchema);
  },

  down: async (queryInterface: QueryInterface) => {
    queryInterface.dropTable(ChatGuestTableName);
  },
};
