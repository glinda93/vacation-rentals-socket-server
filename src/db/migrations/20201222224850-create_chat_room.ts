import { DataTypes, QueryInterface } from "sequelize";
import { ChatRoomSchema, ChatRoomTableName } from "../models";

export default {
  up: async (queryInterface: QueryInterface) => {
    queryInterface.createTable(ChatRoomTableName, ChatRoomSchema);
  },
  down: async (queryInterface: QueryInterface) => {
    queryInterface.dropTable("chat_rooms");
  },
};
