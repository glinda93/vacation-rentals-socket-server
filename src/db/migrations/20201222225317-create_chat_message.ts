import { DataTypes, QueryInterface } from "sequelize";
import { ChatMessageSchema, ChatMessageTableName } from "../models";

export default {
  up: async (queryInterface: QueryInterface) => {
    queryInterface.createTable(ChatMessageTableName, ChatMessageSchema, {
      charset: "utf8mb4",
      collate: "utf8mb4_general_ci",
    });
  },
  down: async (queryInterface: QueryInterface) => {
    queryInterface.dropTable(ChatMessageTableName);
  },
};
