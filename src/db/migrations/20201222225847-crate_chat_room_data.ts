import { DataTypes, QueryInterface } from "sequelize";
import { ChatRoomDataSchema, ChatRoomDataTableName } from "../models";

export default {
  up: async (queryInteface: QueryInterface) => {
    queryInteface.createTable(ChatRoomDataTableName, ChatRoomDataSchema);
  },
  down: async (queryInterface: QueryInterface) => {
    queryInterface.dropTable("chat_room_user");
  },
};
