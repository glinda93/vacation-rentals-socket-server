import { ChatRoom } from "./chat-room";
import { ChatRoomData } from "./chat-room-data";
import { ChatMessage } from "./chat-message";

ChatRoom.hasMany(ChatRoomData, {
  as: "chatRoomDatas",
  foreignKey: "roomId",
  onDelete: "CASCADE",
  onUpdate: "CASCADE",
});

ChatRoomData.belongsTo(ChatRoom, {
  foreignKey: "roomId",
});

ChatRoom.hasMany(ChatMessage, {
  as: "chatMessages",
  foreignKey: "roomId",
  onDelete: "CASCADE",
  onUpdate: "CASCADE",
});

ChatMessage.belongsTo(ChatRoom, {
  foreignKey: "roomId",
});
