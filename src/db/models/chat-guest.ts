import { Model, Optional, DataTypes } from "sequelize";
import DatabaseManager from "../../db";

const db = DatabaseManager.getInstance().getDb();

export interface ChatGuestAttributes {
  id: number;
  guestId: string;
  name: string;
  ip: string | null;
  createdAt?: Date | null;
  updatedAt?: Date | null;
}

export interface ChatGuestCreationAttributes extends Optional<ChatGuestAttributes, "id"> {}

export const ChatGuestTableName = "chat_guests";

export const ChatGuestSchema = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  guestId: {
    type: DataTypes.STRING(191),
    allowNull: false,
    field: "guest_id",
    unique: true,
  },
  name: {
    type: DataTypes.STRING(191),
    allowNull: true,
  },
  ip: {
    type: DataTypes.STRING(191),
    allowNull: true,
  },
  createdAt: {
    type: DataTypes.DATE,
    field: "created_at",
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: "updated_at",
  },
};

export class ChatGuest extends Model<ChatGuestAttributes, ChatGuestCreationAttributes> implements ChatGuestAttributes {
  public id: number;
  public guestId: string;
  public name: string;
  public ip: string | null;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public static async getUserInfo(userId: string) {
    const user = await this.findByGuestId(userId);
    if (!user) {
      return null;
    }
    return {
      id: user.guestId,
      name: user["name"],
      profilePicture: "",
    };
  }

  public static async findByGuestId(guestId: string) {
    return await this.findOne({
      where: {
        guestId,
      },
    });
  }
}

ChatGuest.init(ChatGuestSchema, {
  tableName: ChatGuestTableName,
  sequelize: db,
});
