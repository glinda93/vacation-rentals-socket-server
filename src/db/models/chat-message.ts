import { Op } from "sequelize";
import {
  Association,
  BelongsToCreateAssociationMixin,
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  Model,
  DataTypes,
  Optional,
} from "sequelize";
import DatabaseManager from "../../db";
import type { ChatRoom } from "./chat-room";

export interface ChatMessageAttributes {
  id: number;
  roomId: number;
  userId: string;
  message: string;
  createdAt: Date | null;
  updatedAt: Date | null;
}

export interface ChatMessageCreationAttributes extends Optional<ChatMessageAttributes, "id"> {}

export const ChatMessageSchema = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  roomId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    field: "chat_room_id",
  },
  userId: {
    type: DataTypes.STRING(191),
    allowNull: false,
    field: "user_id",
  },
  message: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
  createdAt: {
    type: DataTypes.DATE,
    field: "created_at",
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: "updated_at",
  },
};

export const ChatMessageTableName = "chat_messages";

export class ChatMessage
  extends Model<ChatMessageAttributes, ChatMessageCreationAttributes>
  implements ChatMessageAttributes {
  public id: number;
  public roomId: number;
  public userId: string;
  public message: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public getChatRoom!: BelongsToGetAssociationMixin<ChatRoom>;
  public setChatRoom!: BelongsToSetAssociationMixin<ChatRoom, number>;
  public createChatRoom!: BelongsToCreateAssociationMixin<ChatRoom>;

  public readonly chatRoom?: ChatRoom;

  public static associations: {
    chatRoom: Association<ChatMessage, ChatRoom>;
  };

  public static async getUnreadMessageCount(roomId, userId, lastReadAt = null) {
    let where: any = {
      roomId,
      userId: {
        [Op.ne]: userId,
      },
    };
    if (lastReadAt) {
      where = { ...where, createdAt: { [Op.gt]: lastReadAt } };
    }
    return await this.count({ where });
  }
}

ChatMessage.init(ChatMessageSchema, {
  tableName: ChatMessageTableName,
  sequelize: DatabaseManager.getInstance().getDb(),
  charset: "utf8mb4",
  collate: "utf8mb4_general_ci",
});
