import {
  BelongsToGetAssociationMixin,
  BelongsToSetAssociationMixin,
  BelongsToCreateAssociationMixin,
  Model,
  DataTypes,
  Association,
  Optional,
} from "sequelize";
import * as _ from "lodash";
import DatabaseManager from "../../db";
import type { ChatRoom } from "./chat-room";
import type { ChatMessage } from "./chat-message";
import { ChatMessage as ChatMessageStatic } from "./circular-workaround/chat-message";
import { ContactConnectionType } from "../../types/contact-connection";
import { ParticipantType } from "../../types/participant";
import { Op } from "sequelize";

export interface ChatRoomDataAttributes {
  id: number;
  roomId: number;
  userId: string;
  name: string;
  lastReadAt: Date | null;
}

export interface ChatRoomDataCreationAttributes extends Optional<ChatRoomDataAttributes, "id"> {}

export const ChatRoomDataSchema = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  roomId: {
    type: DataTypes.INTEGER,
    field: "chat_room_id",
  },
  userId: {
    type: DataTypes.STRING(191),
    allowNull: false,
    field: "user_id",
  },
  name: {
    type: DataTypes.STRING(191),
    field: "chat_room_name",
  },
  lastReadAt: {
    type: DataTypes.DATE,
  },
  createdAt: {
    type: DataTypes.DATE,
    field: "created_at",
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: "updated_at",
  },
};

export const ChatRoomDataTableName = "chat_room_user";

export class ChatRoomData
  extends Model<ChatRoomDataAttributes, ChatRoomDataCreationAttributes>
  implements ChatRoomDataAttributes {
  public id: number;
  public roomId: number;
  public userId: string;
  public name: string;
  public lastReadAt: Date | null;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  public getChatRoom!: BelongsToGetAssociationMixin<ChatRoom>;
  public setChatRoom!: BelongsToSetAssociationMixin<ChatRoom, number>;
  public createChatRoom!: BelongsToCreateAssociationMixin<ChatRoom>;

  public readonly chatRoom?: ChatRoom;

  public static associations: {
    chatRoom: Association<ChatMessage, ChatRoom>;
  };

  public static readonly PAGE_LENGTH = 20;

  public static generateRoomName(user: ParticipantType, users: ParticipantType[]) {
    if (users.length < 2) {
      return user.name;
    }
    const otherUsers = users.filter((item) => {
      return item.id != user.id;
    });
    if (!otherUsers.length) {
      return user.name;
    }
    const otherUserRep = otherUsers[0];
    const repName = otherUserRep.name;
    const otherCount = otherUsers.length - 1;
    if (otherCount) {
      return `${repName} and ${otherCount} other(s)`;
    }
    return `${repName}`;
  }

  public static async findByRoomIdAndUserId(roomId: number, userId: string) {
    return await this.findOne({
      where: {
        roomId: {
          [Op.eq]: roomId,
        },
        userId: {
          [Op.eq]: userId,
        },
      },
    });
  }
  public static async getRoomDataByUserId(userId: string, lastCursor = null) {
    return await this.findAll({
      order: [
        ["id", "DESC"],
        ["lastReadAt", "DESC"],
      ],
      where: lastCursor
        ? {
            userId: {
              [Op.eq]: userId,
            },
            id: {
              [Op.lt]: lastCursor,
            },
          }
        : {
            userId: {
              [Op.eq]: userId,
            },
          },
      limit: ChatRoomData.PAGE_LENGTH,
    });
  }
  public static async getRoomDataForCursor(userId: string, cursor = null): Promise<ContactConnectionType> {
    const contacts: ChatRoomData[] = await this.getRoomDataByUserId(userId, cursor);

    let lastCursor = null;
    let hasNext = false;
    if (contacts.length) {
      const lastContact = _.last(contacts);
      lastCursor = lastContact.id;
    }
    if (lastCursor) {
      hasNext = (await this.getRoomDataByUserId(userId, lastCursor)).length > 0;
    }

    return {
      contacts,
      lastCursor,
      hasNext,
    };
  }

  public async getUnreadMessageCount() {
    return await ChatMessageStatic.getUnreadMessageCount(this.roomId, this.userId, this.lastReadAt);
  }
}

ChatRoomData.init(ChatRoomDataSchema, {
  tableName: ChatRoomDataTableName,
  sequelize: DatabaseManager.getInstance().getDb(),
});
