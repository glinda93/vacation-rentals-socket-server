import {
  HasManyGetAssociationsMixin,
  HasManyAddAssociationMixin,
  HasManyHasAssociationMixin,
  HasManyCountAssociationsMixin,
  HasManyCreateAssociationMixin,
  Model,
  DataTypes,
  Optional,
  Association,
} from "sequelize";
import { Op } from "sequelize";
import * as _ from "lodash";
import type { ChatRoomData } from "./chat-room-data";
import type { ChatMessage } from "./chat-message";
import { ChatRoomData as ChatRoomDataStatic } from "./circular-workaround/chat-room-data";
import DatabaseManager from "../../db";
import { isEqualIgnoringOrder } from "../../helpers";
import { userService } from "../../services";
import { ParticipantType } from "../../types/participant";
import { MessageConnectionType } from "../../types/message-connection";
import { userManager } from "../../socket/user-manager";
import { UserInfoType } from "../../socket/types";
const debug = require("debug")("chat-room");

export interface ChatRoomAttributes {
  id: number;
  createdAt: Date;
  updatedAt: Date;
}

export interface ChatRoomCreationAttributes extends Optional<ChatRoomAttributes, "id"> {}

export const ChatRoomSchema = {
  id: {
    type: DataTypes.INTEGER,
    allowNull: false,
    autoIncrement: true,
    primaryKey: true,
  },
  createdAt: {
    type: DataTypes.DATE,
    field: "created_at",
  },
  updatedAt: {
    type: DataTypes.DATE,
    field: "updated_at",
  },
};

export const ChatRoomTableName = "chat_rooms";

export class ChatRoom extends Model<ChatRoomAttributes, ChatRoomCreationAttributes> implements ChatRoomAttributes {
  public id!: number;
  public createdAt: Date;
  public updatedAt: Date;

  public getChatRoomDatas!: HasManyGetAssociationsMixin<ChatRoomData>;
  public addChatRoomData!: HasManyAddAssociationMixin<ChatRoomData, number>;
  public hasChatRoomData!: HasManyHasAssociationMixin<ChatRoomData, number>;
  public countChatRoomDatas!: HasManyCountAssociationsMixin;
  public createChatRoomData: HasManyCreateAssociationMixin<ChatRoomData>;

  public getChatMessages!: HasManyGetAssociationsMixin<ChatMessage>;
  public addChatMessage!: HasManyAddAssociationMixin<ChatMessage, number>;
  public hasChatMessage!: HasManyHasAssociationMixin<ChatMessage, number>;
  public countChatMessages!: HasManyCountAssociationsMixin;
  public createChatMessage!: HasManyCreateAssociationMixin<ChatMessage>;

  public readonly chatRoomDatas?: ChatRoomData[];
  public readonly chatMessages?: ChatMessage[];

  public static readonly PAGE_LENGTH = 10;

  public getName() {
    return `room:${this.id}`;
  }

  public static associations: {
    chatRoomDatas: Association<ChatRoom, ChatRoomData>;
    chatMessages: Association<ChatRoom, ChatMessage>;
  };

  public static async findOrCreateByUserIds(userIds: string[]): Promise<ChatRoom | null> {
    const room = await this.findByUserIds(userIds);
    if (room) {
      return room;
    }
    return await this.createFromUserIds(userIds);
  }

  public static async findByUserIds(userIds: string[]) {
    userIds = _.uniqBy(userIds, (userId) => userId);
    if (!userIds.length) {
      console.warn("userIds empty");
      return null;
    }
    const chatRoomDatas: ChatRoomData[] = await ChatRoomDataStatic.findAll({
      where: {
        userId: userIds[0],
      },
    });
    if (chatRoomDatas.length) {
      for (const chatRoomData of chatRoomDatas) {
        const roomId = chatRoomData.roomId;
        const allParticipantDatas = await ChatRoomDataStatic.findAll({
          where: {
            roomId,
          },
        });
        const allParticipantIds = allParticipantDatas.map((item: ChatRoomData) => item.userId);
        if (isEqualIgnoringOrder(allParticipantIds, userIds)) {
          const room = await this.findByPk(roomId);
          if (room) {
            return room;
          }
        }
      }
    }
    return null;
  }

  public static async createFromUserIds(userIds: string[]) {
    userIds = _.uniqBy(userIds, (userId) => userId);
    const users: Array<ParticipantType> = [];
    for (const userId of userIds) {
      const userInfo = await userService.getUserInfo(userId);
      if (userInfo) {
        users.push(userInfo);
      }
    }
    if (!users.length) {
      console.warn("User info empty");
      return null;
    }
    const room = new ChatRoom();
    await room.save();
    for (const user of users) {
      debug("createFromUserIds : " + user.id);
      const roomName = ChatRoomDataStatic.generateRoomName(user, users);
      await room.createChatRoomData({
        name: roomName,
        userId: user.id,
        lastReadAt: null,
      });
    }
    return room;
  }

  public async hasUserId(userId: string) {
    const chatRoomDatas = await this.getChatRoomDatas();
    return (
      chatRoomDatas.findIndex((roomData) => {
        return roomData.userId === userId;
      }) > -1
    );
  }

  public async getChatMessagesBeforeCursor(cursor = null) {
    return await this.getChatMessages({
      order: [
        ["id", "DESC"],
        ["createdAt", "DESC"],
      ],
      where: cursor
        ? {
            id: {
              [Op.lt]: cursor,
            },
          }
        : {},
      limit: ChatRoom.PAGE_LENGTH,
    });
  }

  public async getChatMessageConnection(cursor = null): Promise<MessageConnectionType> {
    const edges: ChatMessage[] = await this.getChatMessagesBeforeCursor(cursor);
    let lastCursor = null;
    let hasNext = false;
    if (edges.length) {
      const lastEdge = _.last(edges);
      lastCursor = lastEdge.id;
    }
    if (lastCursor) {
      hasNext = (await this.getChatMessagesBeforeCursor(lastCursor)).length > 0;
    }
    return {
      edges,
      lastCursor,
      hasNext,
    };
  }

  public async getParticipantIds(): Promise<string[]> {
    const chatRoomDatas = await this.getChatRoomDatas();
    return chatRoomDatas.map(({ userId }) => userId);
  }

  public async getRoomData(userId: string) {
    const roomName = this.getName();
    const roomData = (
      await this.getChatRoomDatas({
        where: {
          userId,
        },
      })
    )[0];
    if (!roomData) {
      return null;
    }
    const messageConnection = await this.getChatMessageConnection();
    const unreadMessageCount = await roomData.getUnreadMessageCount();
    const participantIds = await this.getParticipantIds();
    const participants = await Promise.all(
      _.map(participantIds, async (participantId) => {
        const userInfo: UserInfoType = await userService.getUserInfo(participantId);
        return {
          ...userInfo,
          isOnline: userManager.isInside(participantId, roomName),
        };
      })
    );
    if (!roomData.name) {
      const otherParty = participants.find(({ id }) => id !== userId);
      if (otherParty) {
        roomData.name = otherParty.name;
        await roomData.save();
      }
    }
    return {
      roomName,
      roomData,
      messageConnection,
      unreadMessageCount,
      participants,
    };
  }
}

ChatRoom.init(ChatRoomSchema, {
  tableName: ChatRoomTableName,
  sequelize: DatabaseManager.getInstance().getDb(),
});
