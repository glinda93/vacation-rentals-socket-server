import { DataTypes, Model } from "sequelize";
import DatabaseManager from "../../db";

class ProfilePicture extends Model {
  public userId: string;
  public src: string;
  public photoSource: string;
  public imageLink: string;
}

ProfilePicture.init(
  {
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: "user_id",
      primaryKey: true,
    },
    src: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    photoSource: {
      type: DataTypes.STRING,
      field: "photo_source",
    },
    imageLink: {
      type: DataTypes.VIRTUAL,
      get() {
        const photoSource: string = this.getDataValue("photoSource");
        const src: string = this.getDataValue("src");
        if (["Google", "Facebook", "LinkedIn"]) {
          return src;
        }
        if (src.includes(".")) {
          return `https://blueskyapi.vacation.rentals/images/users/${this.getDataValue("userId")}/${src}`;
        } else {
          return `https://res.cloudinary.com/vacation-rentals/image/upload/c_fit,h_225,w_225/${src}.jpg`;
        }
      },
    },
  },
  {
    tableName: "profile_picture",
    sequelize: DatabaseManager.getInstance().getDb(),
    timestamps: false,
  }
);

export default ProfilePicture;
