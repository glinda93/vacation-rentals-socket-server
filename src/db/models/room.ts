import { DataTypes, Model } from "sequelize";
import DatabaseManager from "../../db";

class Room extends Model {}

Room.init(
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    user_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: "user_id",
    },
    name: {
      type: DataTypes.STRING,
    },
    subname: {
      type: DataTypes.STRING,
    },
  },
  {
    tableName: "rooms",
    timestamps: false,
    sequelize: DatabaseManager.getInstance().getDb(),
  }
);
