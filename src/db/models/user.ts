import { DataTypes, Model } from "sequelize";
import DatabaseManager from "../../db";
import ProfilePicture from "../../db/models/profile-picture";

export class User extends Model {
  public static async getUserInfo(userId: string) {
    if (userId && userId.startsWith("user:")) {
      userId = userId.replace("user:", "");
    }
    const user = await this.findByPk(userId);
    if (!user) {
      return null;
    } else {
      const profilePicture = await ProfilePicture.findOne({
        where: {
          userId,
        },
      });
      return {
        id: `user:${userId}`,
        name: user["name"],
        profilePicture: profilePicture ? profilePicture["imageLink"] : "",
      };
    }
  }
}

User.init(
  {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "first_name",
    },
    lastName: {
      type: DataTypes.STRING,
      field: "last_name",
    },
    name: {
      type: DataTypes.VIRTUAL,
      get() {
        let name = this.getDataValue("firstName");
        const lastName: string = this.getDataValue("lastName");
        if (!lastName) {
          return name;
        }
        if ((name + " " + lastName).length > 25) {
          return name + " " + lastName.charAt(0).toUpperCase() + ".";
        } else {
          return name + " " + lastName;
        }
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      field: "created_at",
    },
    updatedAt: {
      type: DataTypes.DATE,
      field: "updated_at",
    },
  },
  {
    tableName: "users",
    sequelize: DatabaseManager.getInstance().getDb(),
  }
);
