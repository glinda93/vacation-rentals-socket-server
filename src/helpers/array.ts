import * as _ from "lodash";

export const isEqualIgnoringOrder = (arr1: Array<unknown>, arr2: Array<unknown>): boolean => {
  return _.isEqual(arr1.sort(), arr2.sort());
};
