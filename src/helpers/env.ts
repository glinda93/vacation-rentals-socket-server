export const env = (key: string, defaultVal: any = null): any => {
  if (process.env[key] !== undefined) {
    return process.env[key];
  } else {
    return defaultVal;
  }
};
