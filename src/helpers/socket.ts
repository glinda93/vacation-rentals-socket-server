import * as SocketIOClient from "socket.io-client";
import { config } from "../config/helper";

type ConnectOption = {
  token?: string;
  guestId?: string;
};

export const connect = (option: ConnectOption) => {
  const PORT = config("app.port");
  let url = `http://localhost:${PORT}`;
  if (option.token) {
    url = `${url}?token=${encodeURIComponent(option.token)}`;
  }
  if (option.guestId) {
    url = `${url}?guestId=${encodeURIComponent(option.guestId)}`;
  }
  return SocketIOClient.connect(url);
}
