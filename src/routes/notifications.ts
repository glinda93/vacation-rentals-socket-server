import * as express from "express";
import * as SocketIO from "socket.io";
import { SocketEvents } from "../socket/events";

export const NotificationsRouter = express.Router();

NotificationsRouter.post("/", async (req, res) => {
  if (req.body.notifications) {
    const io: SocketIO.Server = res.locals.socketIO;
    req.body.notifications.forEach((n: { user_id: string }) => {
      io.to(`user_${n.user_id}`).emit(SocketEvents.NOTIFICATION, n);
    });
    res.status(200).send({
      success: true,
    });
  } else {
    res.status(422).send({
      success: false,
      message: "Empty notification data",
    });
  }
});