import RoomService from './room-service';
import UserService from './user-service';

export const roomService = RoomService.getInstance();
export const userService = UserService.getInstance();