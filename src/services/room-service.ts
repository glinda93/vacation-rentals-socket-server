class RoomService {
  protected static _INSTANCE: RoomService;
  protected constructor() {}

  public static getInstance(): RoomService {
    if (!this._INSTANCE) {
      this._INSTANCE = new RoomService();
    }
    return this._INSTANCE;
  }
}

export default RoomService;
