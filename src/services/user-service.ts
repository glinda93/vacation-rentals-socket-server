import { ChatGuest, User } from "../db/models";

class UserService {
  protected static _INSTANCE: UserService;
  protected constructor() {}

  public static getInstance(): UserService {
    if (!this._INSTANCE) {
      this._INSTANCE = new UserService();
    }
    return this._INSTANCE;
  }

  public async getUserInfo(userId: string) {
    if (userId.startsWith("user:")) {
      return await User.getUserInfo(userId);
    } else {
      return await ChatGuest.getUserInfo(userId);
    }
  }

  public async saveGuestInfo(guestId: string, guestInfo: { name: string; ip: string }) {
    return await ChatGuest.upsert({
      guestId,
      ...guestInfo,
    });
  }
}

export default UserService;
