import * as SocketIO from "socket.io";
import { DynamicObject, SocketWithPayload } from "./types";

export const debug = require("debug")("socket-server:socket");

export function addDataToSocket(socket: SocketWithPayload, data: DynamicObject) {
  if (!socket.data) {
    socket.data = {};
  }
  socket.data = { ...socket.data, ...data };
}

export function emitTo(io: SocketIO.Server, rooms: string[], event: string, payload: any) {
  if (!rooms.length) {
    return;
  }
  let server = io;
  rooms.forEach((room) => {
    server = server.to(room);
  });
  return server.emit(event, payload);
}
