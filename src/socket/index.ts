import * as SocketIO from "socket.io";
import * as _ from "lodash";
import { SocketWithPayload, UserInfoType } from "./types";
import { SocketEvents } from "./events";
import { guestMiddleware, jwtMiddleware } from "./middlewares";
import { userManager } from "./user-manager";
import { userService } from "../services";
import { ChatGuest, ChatMessage, ChatRoom, ChatRoomData } from "../db/models";
import { Socket } from "socket.io-client";
const debug = require("debug")("socket-server:socket");

export default function initSocket(io: SocketIO.Server) {
  io.use(guestMiddleware);
  io.use(jwtMiddleware);

  io.on("connection", (socket: SocketWithPayload) => {
    const jwtDecoded = _.get(socket, "data.jwtDecoded");
    let userId: string = "";
    if (!jwtDecoded) {
      let guestId = _.get(socket, "data.guestId");
      if (guestId && guestId.startsWith("guest:")) {
        guestId = guestId.replace("guest:", "");
      } else {
        guestId = socket.id;
      }
      userId = `guest:${guestId}`;
      socket.emit(SocketEvents.ASSIGN_GUEST_ID, userId);
    } else {
      userId = `user:${jwtDecoded.sub}`;
      socket.emit(SocketEvents.ASSIGN_USER_ID, userId);
    }
    debug(`Joining user in Room ID: ${userId}`);
    io.sockets.emit(SocketEvents.ONLINE, userId);
    userManager.addUser(userId, socket.id);
    socket.join(userId);

    socket.on("disconnect", () => {
      debug(`User with ID: ${userId} disconnected`);
      const user = userManager.getUser(userId);
      if (user) {
        const { rooms } = user;
        rooms.forEach((room) => {
          socket.to(room).emit(SocketEvents.OFFLINE, userId);
        });
      }
      userManager.removeUser(userId);
    });

    socket.on(SocketEvents.GET_ONLINE_STATUS, (userId) => {
      socket.emit(SocketEvents.SET_ONLINE_STATUS, { userId, online: userManager.exists(userId) });
    });

    socket.on(SocketEvents.SEND_MESSAGE, async ({ roomName: roomId, message }) => {
      if (roomId && String(roomId).startsWith("room:")) {
        roomId = roomId.replace("room:", "");
      }
      const room = await ChatRoom.findByPk(roomId);
      if (!room) {
        debug(`Room does not exist for id: ${roomId}`);
        return;
      }
      if (!room.hasUserId(userId)) {
        debug(`User is not joined in room id: ${roomId}`);
        return;
      }
      const chatMessage = new ChatMessage({
        userId,
        roomId,
        message,
        createdAt: new Date(),
        updatedAt: new Date(),
      });
      await chatMessage.save();
      await room.reload();
      const roomName = room.getName();
      debug(`Emit NEW_MESSAGE to ${roomName}`);
      io.of("/").to(roomName).emit(SocketEvents.NEW_MESSAGE, chatMessage);
      const participantIds = await room.getParticipantIds();
      const onlineInactiveParticipantSockets = participantIds
        .filter((participantId) => {
          return userManager.exists(participantId) && !userManager.isInside(participantId, roomName);
        })
        .map((participantId) => {
          return userManager.getUser(participantId);
        });
      debug("participantIds count : " + participantIds.length);
      debug("onlineInteractiveSockets count : " + onlineInactiveParticipantSockets.length);
      for (const { userId: participantId, socketId: participantSocketId } of onlineInactiveParticipantSockets) {
        const roomData = await room.getRoomData(participantId);
        debug(`Emit SET_ROOM_DATA to participant: ${participantId}, socket ID: ${participantSocketId}`);
        io.to(participantSocketId).emit(SocketEvents.SET_ROOM_DATA, roomData);
        io.of("/").sockets.get(participantSocketId).join(roomName);
        userManager.joinRoom(participantId, roomName);
      }
      debug("xxx---sendMessage...");
    });

    socket.on(SocketEvents.SET_USERNAME, async (payload: { name: string }) => {
      if (!userId.startsWith("guest:")) {
        debug(`User ${userId} is not a guest`);
        return;
      }
      const chatGuest = await ChatGuest.findByGuestId(userId);
      if (!chatGuest) {
        debug(`Cannot find guest with ID: ${userId}`);
        return;
      }
      chatGuest.name = payload.name;
      await chatGuest.save();
      socket.emit(SocketEvents.SET_USERNAME, { name: chatGuest.name });
    });

    socket.on(SocketEvents.INIT_CHAT, async (payload: { otherPartyId: string; guest?: { name: string } }) => {
      const { otherPartyId } = payload;
      if (userId.startsWith("guest:")) {
        await userService.saveGuestInfo(userId, {
          ...payload.guest,
          ip: socket.handshake.headers["x-forwarded-for"] || socket.conn.remoteAddress.split(":")[3],
        });
      }
      const room = await ChatRoom.findOrCreateByUserIds([userId, otherPartyId]);
      const roomName = room.getName();
      socket.join(roomName);
      userManager.joinRoom(userId, roomName);
      userManager.setActiveRoom(userId, roomName);
      const roomData = await room.getRoomData(userId);
      debug("xxx---init_chat...");
      socket.emit(SocketEvents.SET_ROOM_DATA, roomData);
    });

    socket.on(SocketEvents.LEAVE_CHAT, async (roomName) => {
      socket.leave(roomName);
      userManager.leaveRoom(userId, roomName);
      socket.to(roomName).emit(SocketEvents.USER_LEFT, { roomName, participantId: userId });
    });

    socket.on(SocketEvents.SET_ACTIVE_ROOM, async (roomName: string) => {
      const roomId = parseInt(roomName.replace("room:", ""));
      const socketUser = userManager.getUser(userId);
      if (!socketUser) {
        debug(`socket user empty for roomId: ${roomId}, userId: ${userId}`);
        return;
      }
      const chatRoomData = await ChatRoomData.findByRoomIdAndUserId(roomId, userId);
      if (chatRoomData) {
        chatRoomData.lastReadAt = new Date();
        await chatRoomData.save();
      } else {
        debug(`Cannot find room data with room id: ${roomId} and user id: ${userId}`);
      }
      roomName = `room: ${roomId}`;
      if (userManager.isActive(userId, roomName)) {
        debug(`Room ${roomName}, userId: ${userId} already active`);
      } else {
        userManager.setActiveRoom(userId, roomName);
        const userInfo = await userService.getUserInfo(userId);
        socket.to(roomName).emit(SocketEvents.JOIN_CHAT, userInfo);
      }
    });

    socket.on(SocketEvents.GET_MESSAGE_CONNECTION, async (payload: { roomName: string; cursor: null | string }) => {
      const { roomName, cursor } = payload;
      const roomId = parseInt(roomName.replace("room:", ""));
      const socketUser = userManager.getUser(userId);
      if (!socketUser) {
        debug(`socket user empty for roomId: ${roomId}, userId: ${userId}`);
        return;
      }
      const room = await ChatRoom.findByPk(roomId);
      if (!room) {
        debug(`Room does not exist for id: ${roomId}`);
        return;
      }
      if (!room.hasUserId(userId)) {
        debug(`User is not joined in room id: ${roomId}`);
        return;
      }
      const connection = await room.getChatMessageConnection(cursor);
      socket.emit(SocketEvents.SET_MESSAGE_CONNECTION, { roomName: room.getName(), connection });
    });

    socket.on(SocketEvents.GET_ROOM_DATA, async ({ roomName }: { roomName: string }) => {
      const roomId = parseInt(roomName.replace("room:", ""));
      const room = await ChatRoom.findByPk(roomId);
      if (!room) {
        debug(`Cannot find room with id: ${roomId}`);
        return;
      }
      if (!room.hasUserId(userId)) {
        debug(`User is not joined in room id: ${roomId}`);
        return;
      }
      const roomData = await room.getRoomData(userId);
      roomName = room.getName();
      if (!userManager.isInside(userId, roomName)) {
        socket.join(roomName);
        userManager.joinRoom(userId, roomName);
      }
      debug("xxx---getRoomData...");
      socket.emit(SocketEvents.SET_ROOM_DATA, roomData);
    });

    socket.on(SocketEvents.GET_CONTACTS, async ({ lastCursor }: { lastCursor: string | null }) => {
      const contactData = await ChatRoomData.getRoomDataForCursor(userId, lastCursor);
      const roomDatas = contactData.contacts;
      let contacts = [];
      for (const roomItem of roomDatas) {
        let roomId = roomItem.roomId;
        const room = await ChatRoom.findByPk(roomId);
        if (!room) {
          debug(`Cannot find room with id: ${roomId}`);
          return;
        }
        const roomData = await room.getRoomData(userId);
        const roomName = room.getName();
        if (!userManager.isInside(userId, roomName)) {
          socket.join(roomName);
          userManager.joinRoom(userId, roomName);
        }
        contacts.push(roomData);
      }

      contactData.contacts = contacts;

      socket.emit(SocketEvents.SET_CONTACTS, contactData);
    });
  });
}
