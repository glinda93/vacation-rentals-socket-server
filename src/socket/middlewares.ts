import * as jwt from "jsonwebtoken";
import * as _ from "lodash";
import { ExtendedError } from "socket.io/dist/namespace";
import { config } from "../config/helper";
import { debug, addDataToSocket } from "./helpers";
import { SocketWithPayload } from "./types";

const getTokenFromHandshake = (socket: SocketWithPayload) => {
  let token = _.get(socket, "handshake.query.token");
  if (token) {
    return token as string;
  }
  for (const key in socket.handshake.query) {
    if (key) {
      return key as string;
    }
  }
  return undefined;
};

export function jwtMiddleware(socket: SocketWithPayload, next: (err?: ExtendedError) => void): void {
  const token: string | undefined = getTokenFromHandshake(socket);
  if (!token) {
    next();
    return;
  }
  jwt.verify(token, config("app.jwt_secret"), (err, decoded) => {
    if (err) {
      debug(`Authentication failed for token: ${token}`);
      next();
      return;
    }
    addDataToSocket(socket, { jwtDecoded: decoded });
    next();
    return;
  });
}

export function guestMiddleware(socket: SocketWithPayload, next: (err?: ExtendedError) => void): void {
  const guestId: string = _.get(socket, "handshake.query.guestId");
  if (!guestId) {
    next();
    return;
  }
  addDataToSocket(socket, { guestId });
  next();
  return;
}
