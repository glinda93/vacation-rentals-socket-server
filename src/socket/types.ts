import { Socket } from 'socket.io';

export type DynamicObject = {
  [key: string]: any;
};

export type SocketPayload = {
  data: DynamicObject;
};

export type UserInfoType = {
  id: string;
  name: string;
  profilePicture: string;
  isOnline?: boolean;
};

export type SocketWithPayload = Socket & SocketPayload;