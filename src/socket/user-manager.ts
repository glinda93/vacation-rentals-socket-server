import * as _ from "lodash";

export type SocketUser = {
  userId: string;
  socketId: string;
  rooms: string[];
  activeRoom: string;
};

class UserManager {
  protected static _INSTANCE: UserManager;
  protected users: SocketUser[];
  protected constructor() {
    this.users = [];
  }
  static getInstance(): UserManager {
    if (!this._INSTANCE) {
      this._INSTANCE = new UserManager();
    }
    return this._INSTANCE;
  }
  public addUser(userId: string, socketId: string) {
    if (!this.exists(userId)) {
      this.users.push({
        userId,
        socketId,
        rooms: [userId],
        activeRoom: "",
      });
    } else {
      const socketUserIdx = this.findUserIndex(userId);
      const socketUser = this.users[socketUserIdx];
      socketUser.socketId = socketId;
    }
  }
  public removeUser(userId: string) {
    _.remove(this.users, (item) => item.userId === userId);
  }
  public getUser(userId: string): SocketUser | null {
    const index = this.findUserIndex(userId);
    if (index > -1) {
      return this.users[index];
    }
    return null;
  }
  public getUserBySocketId(socketId: string): SocketUser | null {
    const index = this.users.findIndex(({ socketId: id }) => id === socketId);
    if (index > -1) {
      return this.users[index];
    }
    return null;
  }
  public exists(userId: string): boolean {
    return this.findUserIndex(userId) > -1;
  }
  public joinRoom(userId: string, roomId: string) {
    const user = this.getUser(userId);
    if (!user) {
      return;
    }
    if (!user.rooms.includes(roomId)) {
      user.rooms.push(roomId);
    }
  }
  public leaveRoom(userId: string, roomId: string) {
    const user = this.getUser(userId);
    if (!user) {
      return;
    }
    _.remove(user.rooms, (item) => item === roomId);
  }
  public isInside(userId: string, roomId: string): boolean {
    const user = this.getUser(userId);
    if (!user) {
      return false;
    }
    return user.rooms.includes(roomId);
  }
  public isActive(userId: string, roomId: string): boolean {
    const user = this.getUser(userId);
    if (!user) {
      return false;
    }
    return user.activeRoom == roomId;
  }
  public setActiveRoom(userId: string, roomId: string) {
    const user = this.getUser(userId);
    if (!user) {
      return;
    }
    if (!user.rooms.includes(roomId)) {
      return;
    }
    user.activeRoom = roomId;
  }
  protected findUserIndex(userId: string): number {
    return this.users.findIndex(({ userId: id }) => id === userId);
  }
}

export default UserManager;

export const userManager = UserManager.getInstance();