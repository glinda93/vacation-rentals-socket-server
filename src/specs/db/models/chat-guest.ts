import { expect } from "chai";
import {ChatGuest} from "../../../db/models";
import factory from "../../../db/factory";
import DatabaseManager from "../../../db";


describe("ChatGuest", () => {
  afterEach(async () => {
    await ChatGuest.truncate();
  });

  it("is a Model", async () => {
    let model: ChatGuest = await factory.build("ChatGuest");
    expect(model).instanceOf(ChatGuest);
  });

  it("can be persisted", async () => {
    let model: ChatGuest = await factory.build("ChatGuest");
    model = await model.save();
    const saved = await ChatGuest.findOne();
    expect(saved).not.be.empty;
  });
});
