import { expect } from "chai";
import { ChatRoom, ChatGuest, ChatRoomData } from "../../../db/models";
import factory from "../../../db/factory";

describe("ChatRoomData", () => {
  afterEach(async () => {
    await ChatGuest.truncate();
    await ChatRoomData.truncate();
    await ChatRoom.sequelize.query("SET FOREIGN_KEY_CHECKS = 0", null);
    await ChatRoom.truncate();
    await ChatRoom.sequelize.query("SET FOREIGN_KEY_CHECKS = 1", null);
  });

  it("belongs to a chat room", async () => {
    const chatRoomData: ChatRoomData = await factory.create("ChatRoomData");
    const chatRoom: ChatRoom = await chatRoomData.getChatRoom();
    expect(chatRoom).to.not.be.null;
    expect(chatRoom.id).to.exist;
  });
});
