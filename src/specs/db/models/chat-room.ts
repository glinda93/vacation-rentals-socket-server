import { expect } from "chai";
import { userService } from "../../../services";
import factory from "../../../db/factory";
import { ChatRoom, ChatGuest, ChatRoomData, ChatMessage } from "../../../db/models";
import { ParticipantType } from "../../../types/participant";

describe("ChatRoom", () => {
  afterEach(async () => {
    await ChatGuest.truncate();
    await ChatRoomData.truncate();
    await ChatMessage.truncate();
    await ChatRoom.sequelize.query("SET FOREIGN_KEY_CHECKS = 0", null);
    await ChatRoom.truncate();
    await ChatRoom.sequelize.query("SET FOREIGN_KEY_CHECKS = 1", null);
  });

  it("is a Model", async () => {
    let model: ChatRoom = await factory.build("ChatRoom");
    expect(model).instanceOf(ChatRoom);
  });

  it("has ChatRoomDatas", async () => {
    const user1: ChatGuest = await factory.build("ChatGuest");
    const user2: ChatGuest = await factory.build("ChatGuest");
    await factory.create("ChatRoom");
    const room: ChatRoom = await ChatRoom.findOne();
    expect(room).to.be.instanceOf(ChatRoom);
    const chatRoomData1: ChatRoomData = new ChatRoomData({
      roomId: room.id,
      userId: user1.guestId,
      name: user2.name,
      lastReadAt: new Date(),
    });
    const chatRoomData2: ChatRoomData = new ChatRoomData({
      roomId: room.id,
      userId: user2.guestId,
      name: user1.name,
      lastReadAt: new Date(),
    });
    await chatRoomData1.save();
    await chatRoomData2.save();
    await room.reload();
    const chatRoomDatas: ChatRoomData[] = await room.getChatRoomDatas();
    expect(chatRoomDatas.length).to.eq(2);
    expect(chatRoomDatas[0]).to.be.instanceOf(ChatRoomData);
  });

  describe("generateRoomName", () => {
    const userOne: ParticipantType = {
      id: "user:1",
      name: "User 1",
      profilePicture: "",
    };
    const userTwo: ParticipantType = {
      id: "user:2",
      name: "User 2",
      profilePicture: "",
    };
    const userThree: ParticipantType = {
      id: "user:3",
      name: "User 3",
      profilePicture: "",
    };
    context("when there are no other users", () => {
      it("should return self name", () => {
        const expected = userOne.name;
        const actual = ChatRoomData.generateRoomName({ ...userOne }, [{ ...userOne }]);
        expect(actual).to.eql(expected);
      });
    });
    context("when there are another user", () => {
      it("should return the other user's name", () => {
        const expected = userTwo.name;
        const actual = ChatRoomData.generateRoomName({ ...userOne }, [{ ...userOne }, { ...userTwo }]);
        expect(actual).to.eql(expected);
      });
    });
    context("when there are more than two users", () => {
      it("should prepend extra user count to room name", () => {
        const expected = `${userTwo.name} and 1 other(s)`;
        const actual = ChatRoomData.generateRoomName({ ...userOne }, [
          { ...userOne },
          { ...userTwo },
          { ...userThree },
        ]);
        expect(actual).to.eql(expected);
      });
    });
  });

  describe("findOrCreateByUserIds", () => {
    context("when there are no participants", () => {
      it("should return null", async () => {
        const room = await ChatRoom.findOrCreateByUserIds([]);
        expect(room).to.be.null;
      });
    });
    context("when there is a room existing", () => {
      it("should return persisted room", async () => {
        const chatGuest1: ChatGuest = await factory.create("ChatGuest");
        const chatGuest2: ChatGuest = await factory.create("ChatGuest");
        const participants: ParticipantType[] = [];
        participants.push(await userService.getUserInfo(chatGuest1.guestId));
        participants.push(await userService.getUserInfo(chatGuest2.guestId));
        const participantIds = participants.map(({ id }) => id);
        const expected: ChatRoom = await ChatRoom.createFromUserIds(participantIds);

        const actual: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds);
        expect(actual).not.to.be.null;
        expect(actual.id).to.eq(expected.id);
      });
    });
    context("when there is no such room", () => {
      it("should create a new room", async () => {
        const chatGuests: ChatGuest[] = [
          await factory.create("ChatGuest"),
          await factory.create("ChatGuest"),
          await factory.create("ChatGuest"),
        ];
        const participants: ParticipantType[] = [];
        for (const user of chatGuests) {
          participants.push(await userService.getUserInfo(user.guestId));
        }
        const participantIds = participants.map(({ id }) => id);
        const created: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds);

        const actual: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds.slice(0, 2));
        expect(actual.id).to.exist;
        expect(actual.id).not.eq(created.id);
      });
    });
  });

  describe("getChatMessageConnection", () => {
    context("when there are no messages", () => {
      it("should return hasNext false, edges empty array, lastCursor null", async () => {
        const chatGuests: ChatGuest[] = [
          await factory.create("ChatGuest"),
          await factory.create("ChatGuest"),
          await factory.create("ChatGuest"),
        ];
        const participants: ParticipantType[] = [];
        for (const user of chatGuests) {
          participants.push(await userService.getUserInfo(user.guestId));
        }
        const participantIds = participants.map(({ id }) => id);
        const room: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds);
        const connection = await room.getChatMessageConnection();
        expect(connection).to.eql({
          edges: [],
          hasNext: false,
          lastCursor: null,
        });
      });
    });
    context("when there are messages", () => {
      it("should return a correct connection", async () => {
        const chatGuests: ChatGuest[] = [await factory.create("ChatGuest"), await factory.create("ChatGuest")];
        const participants: ParticipantType[] = [];
        for (const user of chatGuests) {
          participants.push(await userService.getUserInfo(user.guestId));
        }
        const participantIds = participants.map(({ id }) => id);
        const room: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds);
        const chatMessages: ChatMessage[] = await factory.createMany<ChatMessage>("ChatMessage", 20, {
          userId: participantIds[0],
          roomId: room.id,
        });
        var { edges, lastCursor, hasNext } = await room.getChatMessageConnection(null);
        expect(edges.length).to.eql(ChatRoom.PAGE_LENGTH);
        expect(lastCursor).to.be.equal(chatMessages[10].id);
        expect(hasNext).to.be.true;
        var { edges, lastCursor, hasNext } = await room.getChatMessageConnection(lastCursor);
        expect(edges.length).to.eql(ChatRoom.PAGE_LENGTH);
        expect(lastCursor).to.be.equal(chatMessages[0].id);
        expect(hasNext).to.be.false;
      });
    });
  });

  describe("hasUserId", () => {
    it("should return true if user has already joined room; false otherwise", async () => {
      const chatGuests: ChatGuest[] = [await factory.create("ChatGuest"), await factory.create("ChatGuest")];
      const participants: ParticipantType[] = [];
      for (const user of chatGuests) {
        participants.push(await userService.getUserInfo(user.guestId));
      }
      const participantIds = participants.map(({ id }) => id);
      const room: ChatRoom = await ChatRoom.findOrCreateByUserIds(participantIds);
      let hasUser = await room.hasUserId(chatGuests[0].guestId);
      expect(hasUser).to.be.true;
      hasUser = await room.hasUserId("dummy");
      expect(hasUser).to.be.false;
    });
  });
});
