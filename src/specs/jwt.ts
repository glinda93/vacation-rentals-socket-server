import * as assert from "assert";
import * as jwt from "jsonwebtoken";
import { config } from "../config/helper";
const userId = "1";

const jwtToken = jwt.sign({ sub: userId }, config("app.jwt_secret"));
const decoded: any = jwt.verify(jwtToken, config("app.jwt_secret"));
assert.strictEqual(userId, decoded.sub);
