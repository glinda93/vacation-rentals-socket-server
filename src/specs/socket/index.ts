import { expect } from "chai";
import * as faker from "faker";
import { config } from "../../config/helper";
import { Server as SocketIOServer } from "socket.io";
import { SocketEvents } from "../../socket/events";
import initSocket from "../../socket";
import factory from "../../db/factory";
import { ChatGuest, ChatMessage, ChatRoom } from "../../db/models";
import { ParticipantType } from "../../types/participant";
import { MessageConnectionType } from "../../types/message-connection";
import { connect } from "../../helpers/socket";

const PORT = config("app.port");
const server = require("http").createServer();
const io = new SocketIOServer(server);
initSocket(io);
io.listen(PORT);

describe("initSocket", () => {
  let socket: SocketIOClient.Socket;
  after(() => {
    io.close();
  });
  afterEach((done) => {
    if (socket.connected) {
      socket.disconnect();
    }
    done();
  });
  context("on connection", () => {
    it("should emit ASSIGN_GUEST_ID", (done) => {
      const guestId = "guest:dummy";
      socket = connectWithGuestId(guestId);
      socket.on(SocketEvents.ASSIGN_GUEST_ID, (newId) => {
        expect(newId).to.equal(guestId);
        done();
      });
    });
    it("should emit ASSIGN_GUEST_ID with new ID", (done) => {
      socket = connectWithGuestId();
      socket.on(SocketEvents.ASSIGN_GUEST_ID, (newId) => {
        expect(newId).to.not.be.empty;
        expect(newId).to.be.string;
        done();
      });
    });
  });
  context("on GET_ONLINE_STATUS", () => {
    it("should respond with SET_ONLINE_STATUS true", (done) => {
      const guestId = "guest:dummy";
      socket = connectWithGuestId(guestId);
      socket.emit(SocketEvents.GET_ONLINE_STATUS, guestId);
      socket.once(SocketEvents.SET_ONLINE_STATUS, ({ userId, online }: { userId: string; online: boolean }) => {
        expect(userId).to.equal(guestId);
        expect(online).to.be.true;
        done();
      });
    });
  });
  context("on SET_USERNAME", () => {
    it("should set guest name", (done) => {
      const guestId = "guest:dummy";
      socket = connectWithGuestId(guestId);
      socket.once(SocketEvents.ONLINE, () => {
        socket.emit(SocketEvents.INIT_CHAT, { otherPartyId: guestId });
      });
      socket.once(SocketEvents.SET_ROOM_DATA, () => {
        socket.emit(SocketEvents.SET_USERNAME, { name: "Jone Doe" });
        socket.once(SocketEvents.SET_USERNAME, ({ name }: { name: string }) => {
          expect(name).to.equal("Jone Doe");
          done();
        });
      });
    });
  });
  context("on INIT_CHAT", () => {
    it("should create a room, emit SET_ROOM_DATA", async () => {
      const asyncWrapper = () => {
        return new Promise(async (resolve) => {
          const chatGuest: ChatGuest = await factory.build("ChatGuest");
          socket = connectWithGuestId(chatGuest.guestId);
          socket.once(SocketEvents.ONLINE, () => {
            socket.emit(SocketEvents.INIT_CHAT, { otherPartyId: chatGuest.guestId, guest: { name: chatGuest.name } });
          });
          socket.on(
            SocketEvents.SET_ROOM_DATA,
            async ({
              roomName,
              participants: [userInfo, otherPartyInfo],
            }: {
              roomName: string;
              participants: ParticipantType[];
            }) => {
              expect(roomName).to.exist;
              expect(roomName).to.be.string;
              expect(userInfo.id).to.equal(chatGuest.guestId);
              expect(userInfo.name).to.equal(chatGuest.name);
              expect(otherPartyInfo).to.equal(chatGuest.guestId);
              expect(userInfo.id).to.equal(chatGuest.id);

              const chatRoom = await ChatRoom.findByUserIds([userInfo.id]);
              const chatRoomDatas = await chatRoom.getChatRoomDatas();
              expect(chatRoomDatas.length).to.equal(2);
              const chatRoomData = chatRoomDatas[0];
              expect(chatRoomData.name).to.equal(userInfo.name);
              console.log("resolving true");
              resolve(true);
            }
          );
        });
      };
      await asyncWrapper();
    });
  });
  context("on SEND_MESSAGE", () => {
    it("should emit NEW_MESSAGE event", async () => {
      const asyncWrapper = () => {
        return new Promise(async (resolve) => {
          const chatGuest: ChatGuest = await factory.build("ChatGuest");
          const message = faker.lorem.sentence();
          socket = connectWithGuestId(chatGuest.guestId);
          socket.emit(SocketEvents.INIT_CHAT, { otherPartyId: chatGuest.guestId, guest: { name: chatGuest.name } });
          socket.on(SocketEvents.SET_ROOM_DATA, ({ roomName }: { roomName: string }) => {
            socket.emit(SocketEvents.SEND_MESSAGE, { roomName, message });
            socket.on(SocketEvents.NEW_MESSAGE, (chatMessage: ChatMessage) => {
              expect(chatMessage.message).to.equal(message);
              expect(`room:${chatMessage.roomId}`).to.equal(roomName);
              expect(chatMessage.userId).to.equal(chatGuest.guestId);
              resolve(true);
            });
          });
        });
      };
      await asyncWrapper();
    });
  });
  context("on GET_MESSAGE_CONNECTION", () => {
    it("should emit SET_MESSAGE_CONNECTION", async () => {
      const asyncWrapper = () => {
        return new Promise(async (resolve) => {
          const chatGuest: ChatGuest = await factory.build("ChatGuest");
          socket = connectWithGuestId(chatGuest.guestId);
          socket.emit(SocketEvents.INIT_CHAT, { otherPartyId: chatGuest.guestId, guest: { name: chatGuest.name } });
          socket.on(SocketEvents.SET_ROOM_DATA, ({ roomName }: { roomName: string }) => {
            socket.emit(SocketEvents.GET_MESSAGE_CONNECTION, { roomName, cursor: null });
            socket.on(
              SocketEvents.SET_MESSAGE_CONNECTION,
              ({ roomName, connection }: { roomName: string; connection: MessageConnectionType }) => {
                expect(typeof roomName).to.equal("string");
                expect(connection.edges).to.be.instanceOf(Array);
                expect(typeof connection.hasNext).to.equal("boolean");
                expect(connection.lastCursor).to.be.null;
                resolve(true);
              }
            );
          });
        });
      };
      await asyncWrapper();
    });
  });
});

function connectWithGuestId(guestId = "") {
  return connect({
    guestId,
  });
}
