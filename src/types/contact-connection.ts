export type ContactConnectionType = {
  contacts: Array<any>;
  lastCursor: null | string;
  hasNext: boolean;
};
