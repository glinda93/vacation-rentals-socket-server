export type MessageConnectionType = {
  edges: Array<any>;
  lastCursor: null|string;
  hasNext: boolean;
}