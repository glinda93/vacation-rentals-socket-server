export type ParticipantType = {
  id: string;
  name: string;
  profilePicture: string;
}